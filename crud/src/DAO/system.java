/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.*;
import connection.listDB;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.connectionDB;
import model.person;

/**
 *
 * @author Manuel
 */
public class system implements operation{
    
    listDB l = new listDB();
    connectionDB conDB = new connectionDB();

    @Override
    public boolean create(person p) {
        Connection con =null;
	Statement s= null;
        
        boolean r = false;
        String sql = "INSERT INTO listpeople VALUES(NULL, '" + p.getName() + "'," + p.getYear() + ",'" + p.getDate() + "','" + p.getGender() + "')";
        try{
            con = l.connexion();
            s = con.createStatement();
            s.execute(sql);
            r = true;
            s.close();
            con.close();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
        return r;
    }

    @Override
    public ArrayList<Object[]> read() {
        Connection con =null;
	Statement s= null;
	ResultSet rs=null;
        
        String sql = "SELECT * FROM listpeople";
        
        ArrayList<Object[]> data = new ArrayList<>();
        
        try{
            con = l.connexion();
            s = con.createStatement();
            rs = s.executeQuery(sql);
            while(rs.next()){
                Object[] row = new Object[5];
                for (int i = 0; i < 5; i++)
                    row[i] = rs.getObject(i + 1);
                data.add(row);
            }
            s.close();
            rs.close();
            con.close();
        }catch (SQLException e){
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
        return data;
    }

    @Override
    public boolean update(person p) {
        Connection con =null;
	Statement s= null;
        
        boolean r = false;
        String sql = "UPDATE listpeople SET name='" + p.getName() + "', year=" + p.getYear() + ", birthday='" + p.getDate() + "', gender='" + p.getGender() + "' WHERE id=" + p.getId();
        try{
            con = l.connexion();
            s = con.createStatement();
            s.execute(sql);
            r = true;
            s.close();
            con.close();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
        return r;
    }

    @Override
    public boolean delete(person p) {
        Connection con =null;
	Statement s= null;
        
        boolean r = false;
        String sql = "DELETE FROM listpeople WHERE id=" + p.getId();
        try{
            con = l.connexion();
            s = con.createStatement();
            s.execute(sql);
            r = true;
            s.close();
            con.close();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage());
        }
        return r;
    }
    
}
