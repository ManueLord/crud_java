/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.person;

/**
 *
 * @author Manuel
 */
public interface operation {
    public boolean create(person p);
    public ArrayList<Object[]> read();
    public boolean update(person p);
    public boolean delete(person p);
}
